package ua.pp.voronin.serhii.task;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CompleteMethods {

    public static void main(String[] args) {
        getUniqueMeasurableItems1(List.of(new Apple(), new WaterMelon(), new Tomato())); // 1
        getUniqueMeasurableItems4(List.of(new Coin(), new Toy(), new Chair())); // 2
        getUniqueMeasurableItems2(List.of(new Water(), new Juice(), new Oil())); // 3
        var packedBeverages = getUniqueMeasurableItems3(List.of(new CanOfCoke(), new BottleOfJuice())); // 4
    }


    // 1
    public static <T extends MeasuredInKilograms> Set<T> getUniqueMeasurableItems1(List<T> list) {
        // some specific processing to happen here
        return new HashSet<T>(list);
    }


    // 2
    public static <T extends MeasuredInLiters> Set<T> getUniqueMeasurableItems2(List<T> list) {
        // some specific processing to happen here
        return new HashSet<T>(list);
    }

    // 3
    public static <T extends MeasuredInLiters & Countable> Set<T> getUniqueMeasurableItems3(List<T> list) {
        // some specific processing to happen here
        return new HashSet<T>(list);
    }


    // 4
    public static <T extends Object> Set<T> getUniqueMeasurableItems4(List<T> list) {
        // some specific processing to happen here
        return new HashSet<T>(list);
    }



    private interface MeasuredInKilograms {}

    private static class Apple implements MeasuredInKilograms {}
    private static class WaterMelon implements MeasuredInKilograms {}
    private static class Tomato implements MeasuredInKilograms {}

    private interface Countable {}

    private static class Coin implements Countable {}
    private static class Toy implements Countable {}
    private static class Chair implements Countable {}

    private interface MeasuredInLiters {}

    private static class Water implements MeasuredInLiters {}
    private static class Juice implements MeasuredInLiters {}
    private static class Oil implements MeasuredInLiters {}

    private static class CanOfCoke implements Countable, MeasuredInLiters {}
    private static class BottleOfJuice implements Countable, MeasuredInLiters {}
}
